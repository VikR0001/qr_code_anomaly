import React, { useState } from 'react';
import {Buffer} from "buffer";
import {Accounts} from 'meteor/accounts-base';

export const Hello = () => {
  const [counter, setCounter] = useState(0);

  const increment = () => {
    setCounter(counter + 1);
  };
  
    Accounts.generate2faActivationQrCode("My app name", (err, svg) => {
        if (err) {
            console.error("Error generating QR code via Meteor", err);
            return;
        }
        console.log(`svg == null: ${svg==null ? 'TRUE' : 'FALSE'}`)
    })



  return (
    <div>
      <button onClick={increment}>Click Me</button>
      <p>You've pressed the button {counter} times.</p>
    </div>
  );
};
